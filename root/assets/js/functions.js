/*------------------------------------------------------------
Hover Menu Contact
------------------------------------------------------------*/
$(window).on("load resize",function(e){
    var width = screen.width;    
    if(width < 768){
        $('.c-header__inner').hide();
        $('.c-dropmenu').show().removeClass('is-open');
        $('.c-header__toggle').on('click', function(e) {
            $(this).toggleClass('is-active');
            $('.c-header').toggleClass('is-active');
            $('.c-header__inner').slideToggle(400);
        });
        $('.c-header__close').on('click', function(e) {
            $('.c-header__toggle').removeClass('is-active');
            $('.c-header').removeClass('is-active');        
            $('.c-header__inner').slideUp(400);
        });
    }else{
        $('.c-header__inner').show();
        $('.c-dropmenu').hide();
        $('.c-header__contact').hover(function() {
            $(this).children('.c-dropmenu').stop().slideDown(400).addClass('is-open');
        },
        function() {
            $(this).children('.c-dropmenu').stop().slideUp(400).removeClass('is-open');
        });
    }
});


$(document).ready(function(){
    var $status = $('.c-slide1__number');
    var $slickElement = $('.c-slide1 .c-slide1__box');

    $slickElement.on('init reInit afterChange', function(event, slick, currentSlide, nextSlide){
        //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
        var i = (currentSlide ? currentSlide : 0) + 1;
        $status.html('<span>'+0+i+'</span>' + '/' + '<span>'+0+slick.slideCount+'</span>');
    });
    $slickElement.slick({
        dots: false,
        infinite: true,
        speed: 300,
        slidesToShow: 2,
        slidesToScroll: 1,
        variableWidth: true,
        arrows: true,
        nextArrow: $('.c-slide1__next'),
        prevArrow: $('.c-slide1__prev'),
        responsive: [
        {
            breakpoint: 767,
            settings: {
                arrows: false,
            }
        }]
    });

    /* Tabs
    ------------------------------------------------------------*/
    $('.c-list02__card').addClass('is-active');
    $('.c-tab__nav li').click(function() {
        var opencontent = $(this).data('content');
        $('.c-tab__nav li').removeClass('is-active');
        $(this).addClass('is-active');

        //$('.c-list02__card').removeClass('is-active');
        //$("."+opencontent).addClass('is-active');
        if(opencontent=='all'){
            $('.c-list02__card').addClass('is-active');
        }
        else{
            $('.c-list02__card').removeClass('is-active');
            $('.c-list02__card.'+opencontent).addClass('is-active');
        }
    });
    });

