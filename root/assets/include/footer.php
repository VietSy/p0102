<footer class="c-footer">	
	<div class="c-contact">
		<div class="l-container">
			<div class="c-contact__head">				
				<div class="c-title02">
					<h3>Contact Us<span>ご相談窓口・資料請求</span></h3>
				</div>
				<p>「どんな特徴があるのかもっと詳しく知りたい」「トータルの費用が知りたい」「他社<br class="pc-only">サービスと比較したい」など、お困り事をなんでもご相談ください。</p>
			</div>
			<div class="c-cvArea">
				<div class="c-cvArea__item">
					<div class="c-cvArea__title">
						<div class="c-cvArea__icon">
							<img class="pc-only" src="/assets/img/common/icon_tel.svg" width="20" height="29" alt=""/>
							<img class="sp-only" src="/assets/img/common/icon_tel.svg" width="11" height="15" alt=""/>
						</div>
						<h3>電話でのご相談</h3>
					</div>							
					<div class="c-cvArea__text">								
						<div class="c-cvArea__tel"><a href="tel:0332571141"><span>東京</span>03-3257-1141</a></div>
						<div class="c-cvArea__tel"><a href="tel:0664421329"><span>大阪</span>06-6442-1329</a></div>
						<span class="c-cvArea__time">
							<b>受付時間</b><br>
							月～金9:00～12:00／13:00～17:00<br>
							（土日祝日、年末年始を除く）
						</span>
					</div>
				</div>
				<div class="c-cvArea__item">
					<div class="c-cvArea__title">
						<div class="c-cvArea__icon">
							<img class="pc-only" src="/assets/img/common/icon_mail.svg" width="29" height="23" alt=""/>
							<img class="sp-only" src="/assets/img/common/icon_mail.svg" width="15" height="12" alt=""/>
						</div>
						<h3>メールでのご相談</h3>
					</div>
					<div class="c-cvArea__text">								
						<p>ご質問やご要望など、お気軽にご相談ください。担当者から3営業日以内にご連絡いたします。</p>
					</div>
					<div class="c-btn01">
				        <a href="">お問い合わせフォーム</a>
				    </div>
				</div>
				<div class="c-cvArea__item">
					<div class="c-cvArea__title">
						<div class="c-cvArea__icon">
							<img class="pc-only" src="/assets/img/common/icon_book.svg" width="29" height="29" alt=""/>
							<img class="sp-only" src="/assets/img/common/icon_book.svg" width="15" height="15" alt=""/>
						</div>
						<h3>資料のご請求</h3>
					</div>
					<div class="c-cvArea__text">								
						<p>NSDビジネスイノベーションがご提供する製品・サービスの各種カタログをお申し込みいただけます。</p>						
					</div>
					<div class="c-btn01">
				        <a href="">資料請求フォーム</a>
				    </div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="c-footer__inner">
		<div class="l-container">
			<ul class="c-footer__nav">
				<li><a href="">会社情報</a></li>
				<li><a href="">サイトのご利用にあたって</a></li>
				<li><a href="">個人情報の取り扱いについて</a></li>
				<li><a href="">個人情報保護方針</a></li>
				<li><a href="">株式会社NSD</a></li>				
			</ul>
			<p class="c-footer__copyright">Copyright © 2019 Business Innovation Co., Ltd. <br class="sp-only">All Rights Reserved.</p>
		</div>
	</div>
	<div class="c-backtop">
		<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
		<polygon class="st0" points="43.2,7.4 85.9,50 43.2,92.6 50.3,99.7 92.9,57.1 100,50 92.9,42.9 50.3,0.3 "/>
		<rect y="45" class="st0" width="90.3" height="10"/>
		</svg>
	</div>
</footer>

<link href="/assets/js/slick/slick.css" rel="stylesheet">
<script src="/assets/js/slick/slick.js"></script>
<script src="/assets/js/functions.js"></script>
</body>
</html>