<!DOCTYPE html>
<html lang="ja" id="pagetop">
<head>
<meta charset="UTF-8">
<meta name="format-detection" content="telephone=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<?php include('meta.php'); ?>
<link href="/assets/css/style.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:700" rel="stylesheet">
<script src="/assets/js/jquery-3.3.1.min.js"></script>
<script src="/assets/js/jquery-migrate-3.0.1.min.js"></script>
</head>
<body class="page-<?php echo $pageid; ?>">

<header class="c-header">
	<div class="c-header__logoSP sp-only">
		<h1 class="c-header__logo">
			<a href=""><img src="/assets/img/common/100.png" width="109" height="40" alt=""/></a>
		</h1>
		<div class="c-header__toggle"><span></span></div>
	</div>
	<div class="c-header__inner">
		<h1 class="c-header__logo pc-only">
			<a href=""><img src="/assets/img/common/100.png" width="160" height="60" alt=""/></a>
		</h1>		
		<div class="c-header__nav">
			<ul class="c-header__menu">
				<li><a href="#">製品・サービス</a></li>
				<li><a href="#">導入ガイド</a></li>
				<li><a href="#">お知らせ・セミナー</a></li>
				<li><a href="#">会社情報</a></li>				
			</ul>
			<div class="c-header__contact">
				<div class="c-header__contact--title">お問い合わせ</div>
				<div class="c-dropmenu">
					<div class="l-container c-cvArea">
						<div class="c-cvArea__item">
							<div class="c-cvArea__title">
								<div class="c-cvArea__icon">
									<img class="pc-only" src="/assets/img/common/icon_tel.svg" width="20" height="29" alt=""/>
									<img class="sp-only" src="/assets/img/common/icon_tel.svg" width="11" height="15" alt=""/>
								</div>
								<h3>電話でのご相談</h3>
							</div>							
							<div class="c-cvArea__text">								
								<div class="c-cvArea__tel"><a href="tel:0332571141"><span>東京</span>03-3257-1141</a></div>
								<div class="c-cvArea__tel"><a href="tel:0664421329"><span>大阪</span>06-6442-1329</a></div>
								<span class="c-cvArea__time">
									<b>受付時間</b><br>
									月～金9:00～12:00／13:00～17:00<br>
									（土日祝日、年末年始を除く）
								</span>
							</div>
						</div>
						<div class="c-cvArea__item">
							<div class="c-cvArea__title">
								<div class="c-cvArea__icon">
									<img class="pc-only" src="/assets/img/common/icon_mail.svg" width="29" height="23" alt=""/>
									<img class="sp-only" src="/assets/img/common/icon_mail.svg" width="15" height="12" alt=""/>
								</div>
								<h3>メールでのご相談</h3>
							</div>
							<div class="c-cvArea__text">								
								<p>ご質問やご要望など、お気軽にご相談ください。担当者から3営業日以内にご連絡いたします。</p>
							</div>
							<div class="c-btn01">
						        <a href="">お問い合わせフォーム</a>
						    </div>
						</div>
						<div class="c-cvArea__item">
							<div class="c-cvArea__title">
								<div class="c-cvArea__icon">
									<img class="pc-only" src="/assets/img/common/icon_book.svg" width="29" height="29" alt=""/>
									<img class="sp-only" src="/assets/img/common/icon_book.svg" width="15" height="15" alt=""/>
								</div>
								<h3>資料のご請求</h3>
							</div>
							<div class="c-cvArea__text">								
								<p>NSDビジネスイノベーションがご提供する製品・サービスの各種カタログをお申し込みいただけます。</p>						
							</div>
							<div class="c-btn01">
						        <a href="">資料請求フォーム</a>
						    </div>
						</div>
					</div>
				</div>
			</div>		
		</div>
		<div class="c-header__close"><img src="/assets/img/common/icon_arrow02.svg" width="15" height="15" alt=""/></div>	
	</div>	
</header>
