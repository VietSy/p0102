<?php /*========================================
title
================================================*/ ?>
<div class="c-dev-title1">title</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-title01</div>
<div class="c-title01">	
	<h3><span>Solution Service</span>ソリューションサービス</h3>
</div>

<div class="c-dev-title2">c-title01 c-title01--color1</div>
<div class="c-title01 c-title01--color1">	
	<h3><span>Cloud Service</span>クラウドサービス</h3>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-title02</div>
<div class="c-title02">
	<h3>Feature<span>NSDビジネスイノベーションの特長</span></h3>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-title03</div>
<div class="c-title03">
	<h3><span>About</span>eTransporterとは</h3>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-title04</div>
<div class="c-title04">
	<h3><span>大企業・官公庁</span>への導入実績多数！<br>大容量ファイルを<span>簡単・安全に送受信</span>できるファイル転送システム</h3>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-title05</div>
<div class="c-title05">
	<h3><span>選ばれる理由 1</span></h3>
</div>