<?php /*========================================
slide
================================================*/ ?>
<div class="c-dev-title1">slide</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-slide1</div>
<div class="c-slide1">
	<div class="c-slide1__inner">
		<div class="c-slide1__box">
			<div class="c-slide1__item">
				<span class="c-slide1__tab u-bg1">セキュリティ</span>
				<div class="c-slide1__img"><img src="/assets/img/common/127.jpg" alt=""></div>
				<h3 class="c-slide1__title" style="background-image: url('/assets/img/common/128.jpg');"><span>テレワークに必須！大容量ファイルを簡単・安全に送信するメソッド</span></h3>
				<div class="c-slide1__text">
					<p class="c-slide1__txt">リード文が入ります。この文章はダミーです予めご了承ください。リード文が入ります。この文章はダミーです予めご了承くだささ</p>
					<span class="c-slide1__time">2019.03.01 8時間前</span>
					<p class="c-slide1__cat"><a href="">#タグ</a><span>,</span><a href="">#タグ</a></p>
				</div>
			</div>
			<div class="c-slide1__item">
				<span class="c-slide1__tab u-bg2">業務効率化・コスト削減</span>
				<div class="c-slide1__img"><img src="/assets/img/common/129.jpg" alt=""></div>
				<h3 class="c-slide1__title" style="background-image: url('/assets/img/common/130.jpg');"><span>テレワークに必須！大容量ファイルを簡単・安全に送信するメソッド</span></h3>
				<div class="c-slide1__text">
					<p class="c-slide1__txt">リード文が入ります。この文章はダミーです予めご了承ください。リード文が入ります。この文章はダミーです予めご了承くだささ</p>
					<span class="c-slide1__time">2019.03.01 8時間前</span>
					<p class="c-slide1__cat"><a href="">#タグ</a><span>,</span><a href="">#タグ</a></p>
				</div>
			</div>
			<div class="c-slide1__item">
				<span class="c-slide1__tab u-bg3">スマートデバイス活用</span>
				<div class="c-slide1__img"><img src="/assets/img/common/127.jpg" alt=""></div>
				<h3 class="c-slide1__title" style="background-image: url('/assets/img/common/128.jpg');"><span>テレワークに必須！大容量ファイルを簡単・安全に送信するメソッド</span></h3>
				<div class="c-slide1__text">
					<p class="c-slide1__txt">リード文が入ります。この文章はダミーです予めご了承ください。リード文が入ります。この文章はダミーです予めご了承くだささ</p>
					<span class="c-slide1__time">2019.03.01 8時間前</span>
					<p class="c-slide1__cat"><a href="">#タグ</a><span>,</span><a href="">#タグ</a></p>
				</div>
			</div>
		</div>
		<div class="c-slide1__control">
			<div class="c-btn02">
			    <a href="">導入ガイド一覧</a>
			</div>
			<div class="c-slide1__btn">
				<div class="c-slide1__number"><span>01</span>/<span>05</span></div>
				<div class="c-slide1__prev"></div>
				<div class="c-slide1__next"></div>
			</div>
		</div>
	</div>
</div>
