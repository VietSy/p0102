<?php /*========================================
other
================================================*/ ?>
<div class="c-dev-title1">other</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-date</div>
<span class="c-date">2019.03.01 8時間前</span>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-cvArea</div>
<div class="l-container c-cvArea">
	<div class="c-cvArea__item">
		<div class="c-cvArea__title">
			<div class="c-cvArea__icon">
				<img class="pc-only" src="/assets/img/common/icon_tel.svg" width="20" height="29" alt=""/>
				<img class="sp-only" src="/assets/img/common/icon_tel.svg" width="11" height="15" alt=""/>
			</div>
			<h3>電話でのご相談</h3>
		</div>							
		<div class="c-cvArea__text">								
			<div class="c-cvArea__tel"><a href="tel:0332571141"><span>東京</span>03-3257-1141</a></div>
			<div class="c-cvArea__tel"><a href="tel:0664421329"><span>大阪</span>06-6442-1329</a></div>
			<span class="c-cvArea__time">
				<b>受付時間</b><br>
				月～金9:00～12:00／13:00～17:00<br>
				（土日祝日、年末年始を除く）
			</span>
		</div>
	</div>
	<div class="c-cvArea__item">
		<div class="c-cvArea__title">
			<div class="c-cvArea__icon">
				<img class="pc-only" src="/assets/img/common/icon_mail.svg" width="29" height="23" alt=""/>
				<img class="sp-only" src="/assets/img/common/icon_mail.svg" width="15" height="12" alt=""/>
			</div>
			<h3>メールでのご相談</h3>
		</div>
		<div class="c-cvArea__text">								
			<p>ご質問やご要望など、お気軽にご相談ください。担当者から3営業日以内にご連絡いたします。</p>
		</div>
		<div class="c-btn01">
	        <a href="">お問い合わせフォーム</a>
	    </div>
	</div>
	<div class="c-cvArea__item">
		<div class="c-cvArea__title">
			<div class="c-cvArea__icon">
				<img class="pc-only" src="/assets/img/common/icon_book.svg" width="29" height="29" alt=""/>
				<img class="sp-only" src="/assets/img/common/icon_book.svg" width="15" height="15" alt=""/>
			</div>
			<h3>資料のご請求</h3>
		</div>
		<div class="c-cvArea__text">								
			<p>NSDビジネスイノベーションがご提供する製品・サービスの各種カタログをお申し込みいただけます。</p>						
		</div>
		<div class="c-btn01">
	        <a href="">資料請求フォーム</a>
	    </div>
	</div>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-tab</div>
<div class="l-container">
	<div class="c-tab">
		<ul class="c-tab__nav">
			<li class="is-active" data-content="all"><span>すべて</span></li>
			<li data-content="security"><span>セキュリティ</span></li>
			<li data-content="graph"><span>業務効率化<br>コスト削減</span></li>
			<li data-content="device"><span>スマート<br>デバイス活用</span></li>
		</ul>
		<div class="c-list02">
			<div class="c-list02__card security">
				<div class="c-btn05 c-btn05--bg1">セキュリティ</div>
				<div class="c-list02__img">
					<img src="/assets/img/common/106.jpg" alt="">
				</div>
				<div class="c-list02__text">
					<p>ユーザ数無制限の法人向けファイル転送システム</p>
					<span>eTransporter</span>
				</div>
			</div>
			<div class="c-list02__card security">
				<div class="c-btn05 c-btn05--bg1">セキュリティ</div>
				<div class="c-list02__img">
					<img src="/assets/img/common/107.jpg" alt="">
				</div>
				<div class="c-list02__text">
					<p>コラボ特化型のオンラインストレージ</p>
					<span>eTransporter Collabo</span>
				</div>
			</div>
			<div class="c-list02__card security">
				<div class="c-btn05 c-btn05--bg1">セキュリティ</div>
				<div class="c-list02__img">
					<img src="/assets/img/common/108.jpg" alt="">
				</div>
				<div class="c-list02__text">
					<p>メール誤送信防止・暗号化</p>
					<span>CipherCraft/Mail</span>
				</div>
			</div>
			<div class="c-list02__card security">
				<div class="c-btn05 c-btn05--bg1">セキュリティ</div>
				<div class="c-list02__img">
					<img src="/assets/img/common/109.jpg" alt="">
				</div>
				<div class="c-list02__text">
					<p>ファイルサーバーアクセスログ監視ツール</p>
					<span>File Server Audit</span>
				</div>
			</div>
			<div class="c-list02__card security">
				<div class="c-btn05 c-btn05--bg1">セキュリティ</div>
				<div class="c-list02__img">
					<img src="/assets/img/common/110.jpg" alt="">
				</div>
				<div class="c-list02__text">
					<p>次世代型エンドポイントセキュリティ</p>
					<span>SOPHOS</span>
				</div>
			</div>
			<div class="c-list02__card security">
				<div class="c-btn05 c-btn05--bg1">セキュリティ</div>
				<div class="c-list02__img">
					<img src="/assets/img/common/111.jpg" alt="">
				</div>
				<div class="c-list02__text">
					<p>ライセンス販売から構築・運用支援まで幅広く対応</p>
					<span>McAfee製品導入支援サービス</span>
				</div>
			</div>
			<div class="c-list02__card security">
				<div class="c-btn05 c-btn05--bg1">セキュリティ</div>
				<div class="c-list02__img">
					<img src="/assets/img/common/112.jpg" alt="">
				</div>
				<div class="c-list02__text">
					<p>監査対応・内部不正対策　特権ID管理ソリューション</p>
					<span>iDoperation</span>
				</div>
			</div>
			<div class="c-list02__card security">
				<div class="c-btn05 c-btn05--bg1">セキュリティ</div>
				<div class="c-list02__img">
					<img src="/assets/img/common/113.jpg" alt="">
				</div>
				<div class="c-list02__text">
					<p>サーバ・PC操作ログ取得・管理</p>
					<span>iDoperation SC</span>
				</div>
			</div>
			<div class="c-list02__card security">
				<div class="c-btn05 c-btn05--bg1">セキュリティ</div>
				<div class="c-list02__img">
					<img src="/assets/img/common/114.jpg" alt="">
				</div>
				<div class="c-list02__text">
					<p>アクセス制御・二要素認証対応 次世代型シングルサインオン</p>
					<span>Evidian</span>
				</div>
			</div>
			<div class="c-list02__card security">
				<div class="c-btn05 c-btn05--bg1">セキュリティ</div>
				<div class="c-list02__img">
					<img src="/assets/img/common/115.jpg" alt="">
				</div>
				<div class="c-list02__text">
					<p>２要素認証 トークンレス・ワンタイムパスワード</p>
					<span>swivel secure</span>
				</div>
			</div>
			<div class="c-list02__card security">
				<div class="c-btn05 c-btn05--bg1">セキュリティ</div>
				<div class="c-list02__img">
					<img src="/assets/img/common/116.jpg" alt="">
				</div>
				<div class="c-list02__text">
					<p>標的型攻撃対策 メールセキュリティソリューション</p>
					<span>Proofpoint</span>
				</div>
			</div>
			<div class="c-list02__card security">
				<div class="c-btn05 c-btn05--bg1">セキュリティ</div>
				<div class="c-list02__img">
					<img src="/assets/img/common/117.jpg" alt="">
				</div>
				<div class="c-list02__text">
					<p>月額100円（税別）から利用可 クラウドメール誤送信対策</p>
					<span>SPC Mailエスティー</span>
				</div>
			</div>
			<div class="c-list02__card security">
				<div class="c-btn05 c-btn05--bg1">セキュリティ</div>
				<div class="c-list02__img">
					<img src="/assets/img/common/118.jpg" alt="">
				</div>
				<div class="c-list02__text">
					<p>電子証明書ライフサイクル管理</p>
					<span>Venafi</span>
				</div>
			</div>
			<div class="c-list02__card graph">
				<div class="c-btn05 c-btn05--bg2">業務効率化・コスト削減</div>
				<div class="c-list02__img">
					<img src="/assets/img/common/119.jpg" alt="">
				</div>
				<div class="c-list02__text">
					<p>Excelを使った管理業務の効率化・管理コスト削減</p>
					<span>ePower/exDirector</span>
				</div>
			</div>
			<div class="c-list02__card graph">
				<div class="c-btn05 c-btn05--bg2">業務効率化・コスト削減</div>
				<div class="c-list02__img">
					<img src="/assets/img/common/120.jpg" alt="">
				</div>
				<div class="c-list02__text">
					<p>Microsoft SharePointによる企業ポータル導入・移行支援</p>
					<span>SharePoint導入・移行支援サービス</span>
				</div>
			</div>
			<div class="c-list02__card graph">
				<div class="c-btn05 c-btn05--bg2">業務効率化・コスト削減</div>
				<div class="c-list02__img">
					<img src="/assets/img/common/121.jpg" alt="">
				</div>
				<div class="c-list02__text">
					<p>印刷のムダを無くせる多機能プリンタドライバ</p>
					<span>FinePrint</span>
				</div>
			</div>
			<div class="c-list02__card device">
				<div class="c-btn05 c-btn05--bg3">スマートデバイス活用</div>
				<div class="c-list02__img">
					<img src="/assets/img/common/122.jpg" alt="">
				</div>
				<div class="c-list02__text">
					<p>iPadから簡単・セキュアに社内ファイルサーバ検索・閲覧</p>
					<span>FileServerPad</span>
				</div>
			</div>
			<div class="c-list02__card c-list02__card--bg"></div>
			<div class="c-list02__card c-list02__card--bg"></div>
			<div class="c-list02__card c-list02__card--bg"></div>
		</div>
	</div>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-block01</div>
<div class="c-block01">
	<div class="c-block01__img sp-only">
		<img src="/assets/img/common/134.png" alt="">
	</div>
	<div class="c-block01__text">
		<h3 class="c-block01__title">ソリューションの提供からインフラ構築まで</h3>
		<div class="c-block01__btn">
			IT課題解決のベストパートナー
		</div>
		<p>NSDビジネスイノベーションは、日々進化し続けるセキュリティ対策や業務効率化、コスト削減などのIT課題に向けて、コンサルティングからシステム開発、運用サポートまで行い、お客様のベストパートナーとしてあり続けます。</p>
	</div>	
	<div class="c-block01__img pc-only">
		<img src="/assets/img/common/134.png" alt="">
	</div>
	<div class="c-block01__ttl">
		<p>Business</p>
		<p>Innovation</p>
	</div>	
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-block02</div>
<div class="c-block02">
	<div class="c-block02__inner">
		<div class="l-container">
			<div class="c-title02">
				<h3>Feature<span>NSDビジネスイノベーションの特長</span></h3>
			</div>
		</div>
		<div class="c-block02__img">
			<img src="/assets/img/common/133.jpg" alt="">
		</div>	
		<div class="c-block02__box">			
			<div class="c-block02__text">
				<div class="c-block02__txt">
					<h3>セキュリティに関連したソリューションをご提供し、お客様のビジネスを支援いたします。</h3>
					<p>ビジネスのIT課題解決、セキュリティレベルの向上や業務改善のサポートを通じて、お客様の企業価値を高めていくことが私たちの使命です。「ソリューションサービス」「システムインテグレーション」「クラウドサービス」の3分野を軸に、お客様に最適なソリューションを提供します。</p>
				</div>
			</div>
		</div>
				
	</div>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-block03</div>
<div class="c-block03">	
	<div class="c-block03__left">
		<div class="c-block03__img">
			<img src="/assets/img/common/131.png" alt="">
		</div>
	</div>
	<div class="c-block03__right">
		<div class="c-block03__text">
			<h3>Download<span>お役立ち資料ダウンロード</span></h3>
			<p>業務のお悩みや課題の解決に役立つ資料を無料で配布しております。セキュリティ対策や業務効率化、コスト削減など、課題解決に是非お役立てください！</p>
			<div class="c-btn02 c-btn02--orange">
			    <a href="">お役立ち資料一覧</a>
			</div>
		</div>
		<div class="c-block03__img sp-only">
			<img src="/assets/img/common/131_sp.png" width="135" height="135" alt="">
		</div>
	</div>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-block04</div>
<div class="c-block04">	
	<div class="c-block04__inner l-container">
		<div class="c-block04__img"><img src="/assets/img/common/138.png" alt=""></div>
		<h3 class="c-block04__title">
			<span>今なら</span>
			企業セキュリティ対策法が詰まった<br>実践ガイドブックをプレゼント！
		</h3>
		<div class="c-block04__btn">
			<a href="">お役立ち資料ダウンロード</a>
		</div>
	</div>		
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-block05</div>
<div class="c-block05">	
	<div class="l-container">
		<div class="c-block05__card">
			<div class="c-block05__head">
				<h3>ファイル転送システムに運用コストをかけたくない！25文字前後</h3>
				<div class="c-block05__avatar"><img src="/assets/img/common/149.png" alt=""></div>
				<ul>
					<li><span>業種</span>サービス業</li>
					<li><span>部署</span>システム部</li>
					<li><span>規模</span>500名</li>
				</ul>
			</div>
			<div class="c-block05__text01">
				<h3><span>課題</span></h3>
				<p>課題、お悩みの詳細が入りますこの文章はダミーです。 課題、お悩みの詳細が入りますこの文章はダミーです。課題、お悩みの詳細が入りますこの文章はダミーです。課題、お悩みの詳細が入りますこの文章はダミーです。課題、お悩みの詳細が入りますこの文章はダミーです（100文字前後）</p>
			</div>
			<div class="c-block05__text02">
				<h3><span>解決策</span></h3>
				<div class="c-block05__txt">
					<p>解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。（200文字前後）</p>
				</div>
				<div class="c-block05__img">
					<img src="/assets/img/common/150.jpg" alt="">
				</div>
			</div>
			<div class="c-btn04">
				<a href="">詳しく見る</a>
			</div>
		</div>
		<div class="c-block05__card">
			<div class="c-block05__head">
				<h3>ファイル転送システムに運用コストをかけたくない！25文字前後</h3>
				<div class="c-block05__avatar"><img src="/assets/img/common/151.png" alt=""></div>
				<ul>
					<li><span>業種</span>サービス業</li>
					<li><span>部署</span>システム部</li>
					<li><span>規模</span>500名</li>
				</ul>
			</div>
			<div class="c-block05__text01">
				<h3><span>課題</span></h3>
				<p>課題、お悩みの詳細が入りますこの文章はダミーです。 課題、お悩みの詳細が入りますこの文章はダミーです。課題、お悩みの詳細が入りますこの文章はダミーです。課題、お悩みの詳細が入りますこの文章はダミーです。課題、お悩みの詳細が入りますこの文章はダミーです（100文字前後）</p>
			</div>
			<div class="c-block05__text02">
				<h3><span>解決策</span></h3>
				<div class="c-block05__txt">
					<p>解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。（200文字前後）</p>
				</div>
				<div class="c-block05__img">
					<img src="/assets/img/common/150.jpg" alt="">
				</div>
			</div>
			<div class="c-btn04">
				<a href="">詳しく見る</a>
			</div>
		</div>
		<div class="c-block05__card">
			<div class="c-block05__head">
				<h3>ファイル転送システムに運用コストをかけたくない！25文字前後</h3>
				<div class="c-block05__avatar"><img src="/assets/img/common/152.png" alt=""></div>
				<ul>
					<li><span>業種</span>サービス業</li>
					<li><span>部署</span>システム部</li>
					<li><span>規模</span>500名</li>
				</ul>
			</div>
			<div class="c-block05__text01">
				<h3><span>課題</span></h3>
				<p>課題、お悩みの詳細が入りますこの文章はダミーです。 課題、お悩みの詳細が入りますこの文章はダミーです。課題、お悩みの詳細が入りますこの文章はダミーです。課題、お悩みの詳細が入りますこの文章はダミーです。課題、お悩みの詳細が入りますこの文章はダミーです（100文字前後）</p>
			</div>
			<div class="c-block05__text02">
				<h3><span>解決策</span></h3>
				<div class="c-block05__txt">
					<p>解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。（200文字前後）</p>
				</div>
				<div class="c-block05__img">
					<img src="/assets/img/common/150.jpg" alt="">
				</div>
			</div>
			<div class="c-btn04">
				<a href="">詳しく見る</a>
			</div>
		</div>
	</div>		
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-block06</div>
<div class="l-container">
<div class="c-block06">	
	<div class="c-block06__card">
		<div class="c-btn05 c-btn05--bg1">セキュリティ</div>
		<div class="c-block06__img">
			<img src="/assets/img/common/153.jpg" alt="">
		</div>
		<div class="c-block06__text">
			<h3>『働き手不足の時代 その対策は…ルーチンワークの撲滅！』</h3>
			<p>リード文が入ります。この文章はダミーです予めご了承下さいこの文章はダミーです予めご了承下さいこの文章はダミーです予めご了承下さい。（80文字前後）</p>
			<span class="c-date">2019.03.01 8時間前</span>
			<div class="c-btn06"><a href="">ダウンロードする</a></div>
		</div>
	</div>
	<div class="c-block06__card">
		<div class="c-btn05 c-btn05--bg1">セキュリティ</div>
		<div class="c-block06__img">
			<img src="/assets/img/common/153.jpg" alt="">
		</div>
		<div class="c-block06__text">
			<h3>『働き手不足の時代 その対策は…ルーチンワークの撲滅！』</h3>
			<p>リード文が入ります。この文章はダミーです予めご了承下さいこの文章はダミーです予めご了承下さいこの文章はダミーです予めご了承下さい。（80文字前後）</p>
			<span class="c-date">2019.03.01 8時間前</span>
			<div class="c-btn06"><a href="">ダウンロードする</a></div>
		</div>
	</div>
</div>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-block07</div>
<div class="c-block07">
	<div class="c-block07__left">
		<div class="c-block07__text">
		</div>
	</div>
	<div class="c-block07__right">
		<img src="/assets/img/common/154.jpg" alt="">
	</div>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-keyVisual</div>
<div class="c-keyVisual">
	<div class="c-keyVisual__img"><img src="/assets/img/common/136.jpg" alt=""></div>
	<div class="c-keyVisual__text">
		<h3>
			<span>大容量ファイルを</span>
			<span>セキュアに送受信</span>
		</h3>
		<div class="c-keyVisual__btn">
			<a href="">法人向けファイル転送システム</a>
		</div>
		<div class="c-keyVisual__logo">
			<img src="/assets/img/common/137.png" alt="">
		</div>
	</div>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-textImg</div>
<div class="l-container">
	<div class="c-textImg01">
		<div class="c-title04">
			<h3><span>大企業・官公庁</span>への導入実績多数！<br>大容量ファイルを<span>簡単・安全に送受信</span>できるファイル転送システム</h3>
		</div>
		<div class="c-textImg01__inner">
			<div class="c-textImg01__text">
				<p>eTransporter（略称、eTra）は、メールに添付できないような大きなファイルや機密性の高いデータを企業間でセキュアに送信・共有できるファイル転送システムです。<br>監査ログ、ファイル自動暗号化に標準で対応し、オプションでウイルスチェック、キーワードフィルター、上長承認にも対応し、セキュリティ面でも安心してファイルを送信することができます。この文章（200文字前後）</p>
			</div>
			<div class="c-textImg01__img">
				<img src="/assets/img/common/145.jpg" alt="">
			</div>
		</div>
	</div>
</div>