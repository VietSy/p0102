<?php /*========================================
list
================================================*/ ?>
<div class="c-dev-title1">list</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-list01</div>
<div class="l-container">
	<div class="c-list01">
		<div class="c-list01__card">
			<div class="c-list01__text01">
				<div class="c-title01">	
					<h3><span>Solution Service</span>ソリューションサービス</h3>
				</div>
				<p>お客様の業務を理解・分析し、的確な製品・サービスの組み合わせやカスタマイズ提案によって、課題解決いたします。さらに、お客様のニーズやご意見を収集し、新しいソリューションの創出に取り組みます。</p>
			</div>
			<div class="c-list01__img"><img src="/assets/img/common/103.png" alt=""></div>
		</div>
		<div class="c-list01__card">
			<div class="c-list01__img"><img src="/assets/img/common/104.png" alt=""></div>			
			<div class="c-list01__text01">
				<div class="c-title01">	
					<h3><span>System Integration</span>システムインテグレーション</h3>
				</div>
				<p>3,000名を超える技術者集団であるＮＳＤグループの強みを活かし、既存の製品・サービスにとらわれない解決策を、ＮＳＤの各事業部と連携してご提案いたします。</p>
			</div>
		</div>
		<div class="c-list01__card">			
			<div class="c-list01__text01">
				<div class="c-title01 c-title01--color1">	
					<h3><span>Cloud Service</span>クラウドサービス</h3>
				</div>
				<p>ソリューションサービスのクラウド化を通じて、スピードが重視されるビジネスニーズや多様なシステム環境にお応えいたします。</p>
			</div>
			<div class="c-list01__img"><img src="/assets/img/common/105.png" alt=""></div>		
		</div>
	</div>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-list01 c-list01__inner</div>
<div class="l-container">
	<div class="c-list01">
		<div class="c-list01__card">
			<div class="c-title05">
				<h3><span>選ばれる理由 1</span></h3>
			</div>
			<div class="c-list01__inner">
				<div class="c-list01__text02">
					<div class="c-title04">
						<h3>理由1が入ります<span>強調箇所</span>理由1が入ります（20文字前後）</h3>
					</div>
					<p>選ばれる理由1の説明が入ります。この文章はダミーです予めご了承ください。この文章はダミーです予めご了承ください。この文章はダミーです予めご了承ください。この文章はダミーです予めご了承ください。この文章はダミーです予めご了承ください。この文章はダミーです予めご了承ください。この文章はダミーです予めご了承ください。この文章はダミーです予めご了承ください。この文章はダミーです予めご了承ください。（200文字前後）</p>
				</div>
				<div class="c-list01__img"><img src="/assets/img/common/146.jpg" alt=""></div>
			</div>
			<div class="c-btn04">
			    <a href="">詳しく見る</a>
			</div>
		</div>
		<div class="c-list01__card">
			<div class="c-title05">
				<h3><span>選ばれる理由 2</span></h3>
			</div>
			<div class="c-list01__inner">
				<div class="c-list01__img"><img src="/assets/img/common/147.jpg" alt=""></div>
				<div class="c-list01__text02">
					<div class="c-title04">
						<h3>理由2が入ります<span>強調箇所</span>理由2が入ります（20文字前後）</h3>
					</div>
					<p>選ばれる理由2の説明が入ります。この文章はダミーです予めご了承ください。この文章はダミーです予めご了承ください。この文章はダミーです予めご了承ください。この文章はダミーです予めご了承ください。この文章はダミーです予めご了承ください。この文章はダミーです予めご了承ください。この文章はダミーです予めご了承ください。この文章はダミーです予めご了承ください。この文章はダミーです予めご了承ください。（200文字前後）</p>
				</div>				
			</div>
			<div class="c-btn04">
			    <a href="">詳しく見る</a>
			</div>
		</div>
		<div class="c-list01__card">
			<div class="c-title05">
				<h3><span>選ばれる理由 3</span></h3>
			</div>
			<div class="c-list01__inner">
				<div class="c-list01__text02">
					<div class="c-title04">
						<h3>理由3が入ります<span>強調箇所</span>理由3が入ります（20文字前後）</h3>
					</div>
					<p>選ばれる理由3の説明が入ります。この文章はダミーです予めご了承ください。この文章はダミーです予めご了承ください。この文章はダミーです予めご了承ください。この文章はダミーです予めご了承ください。この文章はダミーです予めご了承ください。この文章はダミーです予めご了承ください。この文章はダミーです予めご了承ください。この文章はダミーです予めご了承ください。この文章はダミーです予めご了承ください。（200文字前後）</p>
				</div>
				<div class="c-list01__img"><img src="/assets/img/common/148.jpg" alt=""></div>
			</div>
			<div class="c-btn04">
			    <a href="">詳しく見る</a>
			</div>
		</div>
	</div>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-list02</div>
<div class="l-container">
	<div class="c-list02">
		<div class="c-list02__card">
			<div class="c-btn05 c-btn05--bg1">セキュリティ</div>
			<div class="c-list02__img">
				<img src="/assets/img/common/106.jpg" alt="">
			</div>
			<div class="c-list02__text">
				<p>ユーザ数無制限の法人向けファイル転送システム</p>
				<span>eTransporter</span>
			</div>
		</div>
		<div class="c-list02__card">
			<div class="c-btn05 c-btn05--bg1">セキュリティ</div>
			<div class="c-list02__img">
				<img src="/assets/img/common/107.jpg" alt="">
			</div>
			<div class="c-list02__text">
				<p>コラボ特化型のオンラインストレージ</p>
				<span>eTransporter Collabo</span>
			</div>
		</div>
		<div class="c-list02__card">
			<div class="c-btn05 c-btn05--bg1">セキュリティ</div>
			<div class="c-list02__img">
				<img src="/assets/img/common/108.jpg" alt="">
			</div>
			<div class="c-list02__text">
				<p>メール誤送信防止・暗号化</p>
				<span>CipherCraft/Mail</span>
			</div>
		</div>
		<div class="c-list02__card">
			<div class="c-btn05 c-btn05--bg1">セキュリティ</div>
			<div class="c-list02__img">
				<img src="/assets/img/common/109.jpg" alt="">
			</div>
			<div class="c-list02__text">
				<p>ファイルサーバーアクセスログ監視ツール</p>
				<span>File Server Audit</span>
			</div>
		</div>
		<div class="c-list02__card">
			<div class="c-btn05 c-btn05--bg1">セキュリティ</div>
			<div class="c-list02__img">
				<img src="/assets/img/common/110.jpg" alt="">
			</div>
			<div class="c-list02__text">
				<p>次世代型エンドポイントセキュリティ</p>
				<span>SOPHOS</span>
			</div>
		</div>
		<div class="c-list02__card">
			<div class="c-btn05 c-btn05--bg1">セキュリティ</div>
			<div class="c-list02__img">
				<img src="/assets/img/common/111.jpg" alt="">
			</div>
			<div class="c-list02__text">
				<p>ライセンス販売から構築・運用支援まで幅広く対応</p>
				<span>McAfee製品導入支援サービス</span>
			</div>
		</div>
		<div class="c-list02__card">
			<div class="c-btn05 c-btn05--bg1">セキュリティ</div>
			<div class="c-list02__img">
				<img src="/assets/img/common/112.jpg" alt="">
			</div>
			<div class="c-list02__text">
				<p>監査対応・内部不正対策　特権ID管理ソリューション</p>
				<span>iDoperation</span>
			</div>
		</div>
		<div class="c-list02__card">
			<div class="c-btn05 c-btn05--bg1">セキュリティ</div>
			<div class="c-list02__img">
				<img src="/assets/img/common/113.jpg" alt="">
			</div>
			<div class="c-list02__text">
				<p>サーバ・PC操作ログ取得・管理</p>
				<span>iDoperation SC</span>
			</div>
		</div>
		<div class="c-list02__card">
			<div class="c-btn05 c-btn05--bg1">セキュリティ</div>
			<div class="c-list02__img">
				<img src="/assets/img/common/114.jpg" alt="">
			</div>
			<div class="c-list02__text">
				<p>アクセス制御・二要素認証対応 次世代型シングルサインオン</p>
				<span>Evidian</span>
			</div>
		</div>
		<div class="c-list02__card">
			<div class="c-btn05 c-btn05--bg1">セキュリティ</div>
			<div class="c-list02__img">
				<img src="/assets/img/common/115.jpg" alt="">
			</div>
			<div class="c-list02__text">
				<p>２要素認証 トークンレス・ワンタイムパスワード</p>
				<span>swivel secure</span>
			</div>
		</div>
		<div class="c-list02__card">
			<div class="c-btn05 c-btn05--bg1">セキュリティ</div>
			<div class="c-list02__img">
				<img src="/assets/img/common/116.jpg" alt="">
			</div>
			<div class="c-list02__text">
				<p>標的型攻撃対策 メールセキュリティソリューション</p>
				<span>Proofpoint</span>
			</div>
		</div>
		<div class="c-list02__card">
			<div class="c-btn05 c-btn05--bg1">セキュリティ</div>
			<div class="c-list02__img">
				<img src="/assets/img/common/117.jpg" alt="">
			</div>
			<div class="c-list02__text">
				<p>月額100円（税別）から利用可 クラウドメール誤送信対策</p>
				<span>SPC Mailエスティー</span>
			</div>
		</div>
		<div class="c-list02__card">
			<div class="c-btn05 c-btn05--bg1">セキュリティ</div>
			<div class="c-list02__img">
				<img src="/assets/img/common/118.jpg" alt="">
			</div>
			<div class="c-list02__text">
				<p>電子証明書ライフサイクル管理</p>
				<span>Venafi</span>
			</div>
		</div>
		<div class="c-list02__card">
			<div class="c-btn05 c-btn05--bg2">業務効率化・コスト削減</div>
			<div class="c-list02__img">
				<img src="/assets/img/common/119.jpg" alt="">
			</div>
			<div class="c-list02__text">
				<p>Excelを使った管理業務の効率化・管理コスト削減</p>
				<span>ePower/exDirector</span>
			</div>
		</div>
		<div class="c-list02__card">
			<div class="c-btn05 c-btn05--bg2">業務効率化・コスト削減</div>
			<div class="c-list02__img">
				<img src="/assets/img/common/120.jpg" alt="">
			</div>
			<div class="c-list02__text">
				<p>Microsoft SharePointによる企業ポータル導入・移行支援</p>
				<span>SharePoint導入・移行支援サービス</span>
			</div>
		</div>
		<div class="c-list02__card">
			<div class="c-btn05 c-btn05--bg2">業務効率化・コスト削減</div>
			<div class="c-list02__img">
				<img src="/assets/img/common/121.jpg" alt="">
			</div>
			<div class="c-list02__text">
				<p>印刷のムダを無くせる多機能プリンタドライバ</p>
				<span>FinePrint</span>
			</div>
		</div>
		<div class="c-list02__card">
			<div class="c-btn05 c-btn05--bg3">スマートデバイス活用</div>
			<div class="c-list02__img">
				<img src="/assets/img/common/122.jpg" alt="">
			</div>
			<div class="c-list02__text">
				<p>iPadから簡単・セキュアに社内ファイルサーバ検索・閲覧</p>
				<span>FileServerPad</span>
			</div>
		</div>
		<div class="c-list02__card c-list02__card--bg"></div>
		<div class="c-list02__card c-list02__card--bg"></div>
		<div class="c-list02__card c-list02__card--bg"></div>
	</div>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-list03</div>
<div class="l-container">
	<div class="c-list03">
		<div class="c-list03__card">
			<div class="c-list03__head">
				<span>ニュースリリース</span>
				<p>2019.03.01</p>
			</div>
			<div class="c-list03__text">
				<h3>コーポレートサイト並びにサービスページをリニューアルいたしました</h3>
				<p>リード文が入ります。この文章はダミーです予めご了承ください。リード文が入ります。この文章はダミーです予めご了承ください。...</p>
			</div>
		</div>
		<div class="c-list03__card">
			<div class="c-list03__head">
				<span>ニュースリリース</span>
				<p>2019.03.01</p>
			</div>
			<div class="c-list03__text">
				<h3>役員人事に関するお知らせ</h3>
				<p>リード文が入ります。この文章はダミーです予めご了承ください。リード文が入ります。この文章はダミーです予めご了承ください。...</p>
			</div>
		</div>
		<div class="c-list03__card">
			<div class="c-list03__head">
				<span>製品・サービス</span>
				<p>2019.03.01</p>
			</div>
			<div class="c-list03__text">
				<h3>SOMPOリスクマネジメント株式会社様より弊社分析ソフト「Act-FLOW」を活用した医療インシデント分析ワークショップ「クラウド ImSAFER」提供開始</h3>
				<p>リード文が入ります。この文章はダミーです予めご了承ください。リード文が入ります。この文章はダミーです予めご了承ください。...</p>
			</div>
		</div>
		<div class="c-list03__card">
			<div class="c-list03__head">
				<span>セミナー・イベント</span>
				<p>2019.03.01</p>
			</div>
			<div class="c-list03__text">
				<h3>ヒューマンエラー防止手法セミナー開催のお知らせ</h3>
				<p>【通常コース】2017年11月17日（金）／2018年 3月23日（金）【アドバンスコース】2018年 3月24日（土）</p>
			</div>
		</div>
		<div class="c-list03__card">
			<div class="c-list03__head">
				<span>製品・サービス</span>
				<p>2019.03.01</p>
			</div>
			<div class="c-list03__text">
				<h3>法人向けファイル共有システム「eTransporter Collabo」を販売開始</h3>
				<p>リード文が入ります。この文章はダミーです予めご了承ください。リード文が入ります。この文章はダミーです予めご了承ください。...</p>
			</div>
		</div>
		<div class="c-list03__card">
			<div class="c-list03__head">
				<span>製品・サービス</span>
				<p>2019.03.01</p>
			</div>
			<div class="c-list03__text">
				<h3>法人向けファイル共有システム「eTransporter Collabo」をクラウドで提供</h3>
				<p>リード文が入ります。この文章はダミーです予めご了承ください。リード文が入ります。この文章はダミーです予めご了承ください。...</p>
			</div>
		</div>
	</div>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-list04</div>
<div class="l-container">
	<div class="c-list04">
		<div class="c-list04__card"><img src="/assets/img/common/139.jpg" alt=""></div>
		<div class="c-list04__card"><img src="/assets/img/common/140.jpg" alt=""></div>
		<div class="c-list04__card"><img src="/assets/img/common/141.jpg" alt=""></div>
		<div class="c-list04__card"><img src="/assets/img/common/142.jpg" alt=""></div>
		<div class="c-list04__card"><img src="/assets/img/common/143.jpg" alt=""></div>
		<div class="c-list04__card"><img src="/assets/img/common/144.jpg" alt=""></div>
	</div>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-list05</div>
<div class="l-container">
	<div class="c-list05">
		<div class="c-list05__card">
			<h3 class="c-list05__title">
				<span><img src="/assets/img/common/icon_usability.svg" alt=""></span>
				利便性への<br>あくなき追求
			</h3>
			<p class="c-list05__text">操作性や機能と言った利便性を追求し、開発を行っているため、誰も使い易く、長くご利用頂ける製品となっています。</p>
			<div class="c-btn03">
			    <a href="">便利な機能を見る</a>
			</div>
		</div>
		<div class="c-list05__card">
			<h3 class="c-list05__title">
				<span><img src="/assets/img/common/icon_security2.svg" alt=""></span>
				セキュリティへの<br>こだわり
			</h3>
			<p class="c-list05__text">企業のセキュリティポリシーに合わせ、キメ細かい設定を行うことができ、お客様に安心をお届けいたします。</p>
			<div class="c-btn03">
			    <a href="">セキュリティ機能を見る</a>
			</div>
		</div>
		<div class="c-list05__card">
			<h3 class="c-list05__title">
				<span><img src="/assets/img/common/icon_pdca.svg" alt=""></span>
				管理運用が<br>ラクになる
			</h3>
			<p class="c-list05__text">管理運用がラクになる説明が入ります。この文章はダミーです予めご了承下さい。この文章はダミーです予めご了承下さい。</p>
			<div class="c-btn03">
			    <a href="">管理運用方法を見る</a>
			</div>
		</div>
	</div>
</div>

<?php /*----------------------------------------*/ ?>
<!-- <div class="c-dev-title2">c-list06</div>
<div class="l-container">
	<div class="c-list06">
		<div class="c-list06__card">
			<div class="c-title05">
				<h3><span>選ばれる理由 1</span></h3>
			</div>
			<div class="c-list06__inner">
				<div class="c-list06__inner">
					
				</div>
			</div>
		</div>
	</div>
</div> -->