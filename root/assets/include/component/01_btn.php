<?php /*========================================
btn
================================================*/ ?>
<div class="c-dev-title1">btn</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-btn01</div>
<div style="max-width: 260px">
<div class="c-btn01">
    <a href="">お問い合わせフォーム</a>
</div>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-btn02</div>
<div class="c-btn02">
    <a href="">導入ガイド一覧</a>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-btn03</div>
<div style="max-width: 500px">
<div class="c-btn03">
    <a href="">便利な機能を見る</a>
</div>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-btn04</div>
<div style="max-width: 260px">
<div class="c-btn04">
    <a href="">詳しく見る</a>
</div>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-btn05 c-btn05--bg1</div>
<div class="c-btn05 c-btn05--bg1">セキュリティ</div>

<div class="c-dev-title2">c-btn05 c-btn05--bg2</div>
<div class="c-btn05 c-btn05--bg2">セキュリティ</div>

<div class="c-dev-title2">c-btn05 c-btn05--bg3</div>
<div class="c-btn05 c-btn05--bg3">セキュリティ</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-btn06</div>
<div class="c-btn06"><a href="">ダウンロードする</a></div>