/**
 * PHPの更新
 *   → ブラウザリロード
 *   → phpファイルはHTML構文バリデートできないので、一旦.html出力してからその.htmlをバリデート
 */

const gulp = require("gulp");
const config = require("../config");
const setting = config.setting;
const $ = require("gulp-load-plugins")(config.loadPlugins);

gulp.task("php2html", () => {
	return gulp.src([setting.html.dest + "**/*.php", "!" + setting.html.dest + "assets/include/**/*.php"])
		.pipe($.php2html())
		.pipe(gulp.dest(setting.html.vdest));
});

gulp.task("phpSync", () => {
	return gulp.src(setting.html.dest + "**/*.php").pipe($.browserSync.stream());
});

gulp.task("phpValidate", ['php2html'], () => {
	return gulp.src(setting.html.vdest + "**/*.html")
		.on("error", $.notify.onError("Error: <%= error.message %>"))
		.pipe($.htmlhint(".htmlhintrc"))
		.pipe($.plumber())
		.pipe($.w3cjs())
		.pipe($.htmlhint.failReporter());
});
